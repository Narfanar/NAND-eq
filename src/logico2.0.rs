#![allow(
    unused_imports,
    dead_code,
    unused_assignments,
    unused_mut,
    unused_variables
)]

#![feature(
    type_ascription,
    box_syntax,
    box_patterns,
    inclusive_range_syntax,
    core_intrinsics,
)]

use std::collections::*;
use std::convert::*;
use std::char::*;
use std::option::*;
use std::mem::*;
use std::mem;
use std::cmp::{max, min};
use std::intrinsics;
// use std::collections::*;

mod parser {
    use std::mem::swap;

    fn tag<T>(t: &T) -> u64 {
        unsafe { ::std::intrinsics::discriminant_value(t) }
    }
    
    pub fn vacuum<T>(l: &mut T, mut lost: T) -> T {
        swap(l, &mut lost);
        lost
    }

    #[derive(Copy, Clone, PartialEq, Debug,)]
    pub enum Op {
        Iff,
        Imp,
        Xor,
        Or,
        And,
        Not,
        Nand,
        Nor,
        Xnor,
    }
    
    #[derive(Clone, PartialEq, Debug,)]
    pub enum Token {
        PLeft,
        PRight,
        BLeft,
        BRight,
        Op(Op),
        Var(String),
        Empty,
    }
    impl Token {
        pub fn tok(t: &str) -> (Token, usize) {
            let mut ret = Token::Empty;
            let mut is_var = {
                let mut len = 0;
                let mut list = vec![ ('A' as u8..'Z' as u8 + 1), ('a' as u8..'z' as u8 + 1), ('0' as u8..'9' as u8 + 1) ];
                let mut b =
                        list
                        .clone()[0...1]
                        .iter_mut()
                        .any(|l| l.any(|lc| t.as_bytes()[0] == lc))
                        ;
                
                if b { len += 1; }
                
                for c in t[1..].as_bytes() {
                    b = list
                        .clone()
                        .iter_mut()
                        .any(|l| l.any(|lc| *c == lc))
                        || *c == ('[' as u8) || *c == (']' as u8)
                        ;
                    if b { len += 1; }
                    else { break; }
                }
                len
            };
            
            if vec!["<=>", "<->", "-->", ">-<", "~||"].iter().any(|s| t.starts_with(s)) {
                is_var = 3;
                ret = match &t[0..3] {
                    "<=>" | "<->" => Token::Op(Op::Iff),
                    "-->" => Token::Op(Op::Imp),
                    ">-<" => Token::Op(Op::Xor),
                    "~||" => Token::Op(Op::Nor),
                    _ => Token::Empty,
                };
            }
            else
            if vec!["><", "||", "&&", "~&"].iter().any(|s| t.starts_with(s)) {
                is_var = 2;
                ret = match &t[0..2] {
                    "><" | "!=" | "/=" => Token::Op(Op::Xor),
                    "||" => Token::Op(Op::Or),
                    "&&" => Token::Op(Op::And),
                    "~&" => Token::Op(Op::Nand),
                    _    => Token::Empty,
                };
            }
            else
            if vec!["&", "~", "!", "|", "(", ")", "⇒", "⇔",
                    "¬", "∧", "∨", "⊕"]
                    .iter().any(|s| t.starts_with(s)) {
                is_var = 1;
                ret = match &t[0..1] {
                    "⇔" => Token::Op(Op::Iff),
                    "⇒" => Token::Op(Op::Imp),
                    "∨" => Token::Op(Op::Or),
                    "∧" | "&" => Token::Op(Op::And),
                    "⊕" => Token::Op(Op::Xor),
                    "¬" | "~" | "!" => Token::Op(Op::Not),
                    "|" => Token::Op(Op::Nand),
                    "(" => Token::PLeft,
                    ")" => Token::PRight,
                    _   => Token::Empty,
                }
            }
            else
            if is_var > 0 {
                ret = Token::Var(t[0..is_var].to_owned());
            }
            
            (ret, is_var)
        }
    }
    
    #[derive(Debug, Clone, PartialEq,)]
    pub struct Stmt {
        toks: Vec<Token>,
        ptr: isize,
        len: isize,
    }
    impl Stmt {
        pub fn new() -> Self {
            Stmt{ toks: vec![], ptr: -1, len: 0 }
        }
        pub fn tokenize(&mut self, s: &str) {
            let s: String = s.chars()
                            .filter(
                                |&c| !char::is_whitespace(c as char)
                            )
                            .collect();
            
            self.toks.clear();
            self.ptr = -1;
            self.len = 0;
            let mut i = 0;
            // println!("fn tokenize( i: {}, str_len: {} )", i, s.len());
            while i < s.len() {
                let tok = Token::tok(&s[i..]);
                // println!("[{}, {}]: {:?}", i, s.len(), tok);
                if tok.1 > 0 {
                    i += tok.1;
                    self.len += 1;
                    self.toks.push(tok.0);
                }
                else {
                    break;
                }
            }
        }
        
        pub fn next(&self) -> Option<Token> {
            if self.ptr >= self.len - 1 {
                None
            }
            else {
                Some(self.toks[(1 + self.ptr) as usize].clone())
            }
        }
        
        pub fn expect(&mut self, tok: Token) -> bool {
            let x = self.next();
            
            match x {
                Some(ref t) if t == &tok => self.consume(),
                _ => tok == Token::Empty,
            }
        }
        
        pub fn back(&mut self) {
            if self.ptr >= 0 {
                self.ptr -= 1;
            }
        }
        
        pub fn reset(&mut self) { self.ptr = -1; }
        
        pub fn consume(&mut self) -> bool {
            if self.ptr < self.len - 1 {
                self.ptr += 1;
                true
            }
            else {
                false
            }
        }
    }
    
    pub type S = Box<Exp>;
    
    #[derive(Clone, PartialEq, Debug,)]
    // turn into Vecs instead of binary ops instead at some point.
    pub enum Exp {
        Iff(S, S),
        Imp(S, S),
        Xor_(Vec<Exp>),
        Or_(Vec<Exp>),
        And_(Vec<Exp>),
        Xor(S, S),
        Or(S, S),
        And(S, S),
        Nand(S, S),
        Not(S),
        Var(String),
        Vari(i32),
        True,
        False,
        Empty,
        Invalid,
    }
    
    impl Exp {
        pub fn associative(&self) -> bool {
            match *self {
                Exp::Nand(..) => false, 
                _ => true,
            }
        }
    }
    
    #[derive(Clone, Debug, PartialEq,)]
    pub struct Prop {
        toks: Stmt,
        root: Exp,
    }
    
    impl Prop {
        pub fn from_tokens(s: &Stmt) -> Prop {
            let mut p = Prop { toks: s.clone(), root: Exp::Empty };
            p.build();
            p
        }
        
        pub fn algebraic_normal_form(&mut self) {
            let mut x = Exp::Empty;
            swap(&mut x, &mut self.root);
            Prop::_to_anf(&mut x);
            swap(&mut x, &mut self.root);
        }
        
        fn _to_anf(e: &mut Exp) -> bool {
            use ::parser::Exp::{True, False, Empty, Var, Invalid, And, Or, Xor, Nand, Not, Iff, Imp};
            
            if let Var(..) = *e {
                return false;
            } else if *e == True || *e == False {
                return false;
            }
            let vac = |u: &mut _| vacuum(u, Empty);
            let mut b = false;
            loop {
                // println!("bef: {:?},", e);
                // transformation of current subexpression
                let r = match *e {
                    Not(ref mut x)
                    => {
                        // let mut u = box Exp::Empty;
                        // swap(&mut u, x);
                        Xor(box True, box vacuum(x, Empty))
                    }
                    Xor(box ref mut f, box ref mut s)
                    => {
                        if Prop::_to_anf(f) || Prop::_to_anf(s) {
                            let mut o = Xor(box Empty, box Empty);
                            if let Xor(box ref mut u, box ref mut v) = o {
                                swap(u, f);
                                swap(v, s);
                            }
                            o
                        }
                        else { Empty }
                    },
                    // distribute
                    And(box ref mut f, box ref mut s)
                    => {
                        // println!("[{:?} | {:?}]", f, s);
                        // (x ^ y) & c = x&c ^ y&c
                        if let Xor(ref mut u, ref mut v) = *f {
                            Xor(box And(box vac(u), box s.clone()), box And(box vac(v), box s.clone()))
                        }
                        else
                        if let Xor(box ref mut u, box ref mut v) = *s {
                            Xor(box And(box f.clone(), box vac(u)), box And(box f.clone(), box vac(v)))
                        }
                        else
                        if let True = *f {
                            vac(s)
                        }
                        else
                        if let True = *s {
                            vac(f)
                        }
                        else {
                            Empty
                        }
                    },
                    Or(box ref a, box ref b)
                    => Xor(box Xor(box a.clone(), box b.clone()), box And(box a.clone(), box b.clone())),
                    // FIXME! have to do distribution law stuff...
                    // here
                    _ => Empty,
                };
                
                b |= &r != &Empty;
                
                if r != Empty {
                    *e = r;
                }
                
                if 
                !match *e {
                    And(box ref mut f, box ref mut s)
                    | Xor(box ref mut f, box ref mut s)
                    | Or(box ref mut f, box ref mut s)
                    => { Prop::_to_anf(f) || Prop::_to_anf(s) },
                    
                    Exp::And_(ref mut v)
                    | Exp::Or_(ref mut v)
                    | Exp::Xor_(ref mut v)
                    => {
                        // println!("2nd branch");
                        let mut b = false;
                        for a in v {
                            b |= Prop::_to_anf(a);
                        }
                        b
                    },
                    _ => false,
                }
                { break; }
            }
            // {}
            
            b
        }
        
        pub fn minimal_set_form(&mut self) {
            let mut x = Exp::Empty;
            swap(&mut x, &mut self.root);
            Prop::_to_msf(&mut x);
            swap(&mut x, &mut self.root);
        }
        
        fn _to_msf(r: &mut Exp) -> bool {
            use ::parser::Exp::{Iff, Imp, Xor, Or, And, Not, Var, Empty};
            let _ = match *r {
                Var(_) | Exp::True | Exp::False => {},
                And(box ref mut f, box ref mut s)
                | Or(box ref mut f, box ref mut s)
                    => { Prop::_to_msf(f); Prop::_to_msf(s); },
                Not(box ref mut l) => { Prop::_to_msf(l); },
                Xor(box ref mut f, box ref mut s)
                | Iff(box ref mut f, box ref mut s)
                | Imp(box ref mut f, box ref mut s)
                => {
                    Prop::_to_msf(f);
                    Prop::_to_msf(s);
                },
                _ => {},
            };
            
            let t =
            match *r {
                Xor(box ref f, box ref s)
                => {
                    Or(box And(box f.clone(), box Not(box s.clone())), box And(box Not(box f.clone()), box s.clone()))
                },
                Iff(box ref f, box ref s)
                => {
                    Or(box And(box f.clone(), box s.clone()),
                                    box And(box Not(box f.clone()),
                                    box Not(box s.clone())))
                },
                Imp(box ref f, box ref s)
                => {
                    Or(box Not(box f.clone()), box s.clone())
                },
                ref r => r.clone(),
            };
            
            *r = t;
            true
        }
        
        pub fn minimize(&mut self) {
            let mut x = Exp::Empty;
            swap(&mut x, &mut self.root);
            Prop::minimal(&mut x);
            swap(&mut x, &mut self.root);
        }
        
        fn minimal(e: &mut Exp) -> bool {
            // a general minimizer is probably impossible ATM
            // only negation flattener for now
            use ::parser::Exp::{Empty, Var, Invalid, And, Or, Xor, Nand, Not, Iff, Imp};
            let vac = |u: &mut _| vacuum(u, Empty);
            if let Var(..) = *e {
                return false;
            }
            else if let Not(box ref a) = *e {
                if let Var(_) = *a {
                    return false;
                }
            }
            else if *e == Exp::True || *e == Exp::False {
                return false;
            }
            
            let mut b = false;
            loop {
                let r = match *e {
                    // ~(~(x)) == (x)
                    Exp::Not(box ref n)
                        if tag(n) == tag(&Exp::Not(box Exp::Empty))
                    => {
                        if let Exp::Not(box ref x) = *n {
                            // Prop::minimal(x);
                            x.clone()
                        } else {
                            // Prop::minimal(n);
                            Exp::Not(box n.clone())
                        }
                    },
                    // !(x & y) == !x || !y
                    Exp::Not(box ref a)
                        if tag(a) == tag(&Exp::And(box Empty, box Empty))
                    => {
                        if let Exp::And(ref f, ref s) = *a {
                            Exp::Or(box Exp::Not(f.clone()), box Exp::Not(s.clone()))
                        } else { unreachable!() }
                    },
                    // !(x || y) == !x && !y
                    Exp::Not(box ref a)
                        if tag(a) == tag(&Exp::Or(box Empty, box Empty))
                    => {
                        if let Exp::Or(ref f, ref s) = *a {
                            Exp::And(box Exp::Not(f.clone()), box Exp::Not(s.clone()))
                        } else { unreachable!() }
                    },
                    // ((x) && (x)) == (x)
                    Exp::And(box ref f, box ref s)
                    | Exp::Or(box ref f, box ref s)
                        if *f == *s
                    => f.clone(),
                    // (x && 1) == x
                    Exp::And(box ref f, box ref s)
                        if *f == Exp::True || *s == Exp::True
                    => if *f == Exp::True { s.clone() } else { f.clone() },
                    // x || 0 == x
                    Exp::Or(box ref f, box ref s)
                        if *f == Exp::False || *s == Exp::False
                    => if *f == Exp::False { s.clone() } else { f.clone() },
                    // x || 1 == 1
                    Exp::Or(box ref f, box ref s)
                        if *f == Exp::True || *s == Exp::True
                    => Exp::True,
                    // x && 0 == 0
                    Exp::And(box ref f, box ref s)
                        if *f == Exp::False || *s == Exp::False
                    => Exp::False,
                    Exp::And(box ref mut f, box ref mut s)
                    => {
                        if let Exp::And(box ref mut u, box ref mut v) = *f {
                            if *u == *s {
                                Exp::And( box vac(v), box vac(s) )
                            }
                            else if *v == *s {
                                Exp::And( box vac(u), box vac(s) )
                            }
                            else {
                                Empty
                            }
                        }
                        else if let Exp::And(box ref mut u, box ref mut v) = *s {
                            if *u == *f {
                                Exp::And( box vac(v), box vac(f) )
                            }
                            else if *v == *f {
                                Exp::And( box vac(u), box vac(f) )
                            }
                            else {
                                Empty
                            }
                        }
                        else { Empty }
                    },
                    Exp::Or(box ref mut f, box ref mut s)
                    => {
                        if let Exp::Or(box ref mut u, box ref mut v) = *f {
                            if *u == *s {
                                Exp::Or( box vac(v), box vac(s) )
                            }
                            else if *v == *s {
                                Exp::Or( box vac(u), box vac(s) )
                            }
                            else {
                                Empty
                            }
                        }
                        else if let Exp::Or(box ref mut u, box ref mut v) = *s {
                            if *u == *f {
                                Exp::Or( box vac(v), box vac(f) )
                            }
                            else if *v == *f {
                                Exp::Or( box vac(u), box vac(f) )
                            }
                            else {
                                Empty
                            }
                        }
                        else { Empty }
                    },
                    Exp::Or_(ref mut v)
                    => {
                        // x || 1 == 1
                        if v.contains(&Exp::True) {
                            v.clear();
                            v.push(Exp::True);
                        }
                        // x || 0 == 0
                        else {
                            let mut v2 = v.iter_mut().filter(|t| t != &&Exp::False).map(|t| t.clone()).collect::<Vec<_>>();
                            swap(v, &mut v2);
                        }
                        let mut v2 = vec![];
                        swap(v, &mut v2);
                        Exp::Or_(v2)
                    },
                    Exp::And_(ref mut v)
                    => {
                        // x & 0 == 0
                        if v.contains(&Exp::False) {
                            v.clear();
                            v.push(Exp::False);
                        }
                        // x & 1 == x
                        else {
                            let mut v2 = v.iter_mut().filter(|t| t != &&Exp::True).map(|t| t.clone()).collect() : Vec<_>;
                            swap(v, &mut v2);
                        }
                        let mut v2 = vec![];
                        swap(v, &mut v2);
                        Exp::And_(v2)
                    },
                    Exp::Xor(..)
                    | Exp::Imp(..)
                    | Exp::Nand(..)
                    | Exp::Iff(..)
                    => {
                        // e.clone()
                        Empty
                    },
                    _ => Empty,// e.clone(),
                };
                
                b |= r != Empty;
                
                if r != Empty {
                    *e = r;
                }
                // println!("(*e = r) | {:?}", e);
                // let mut b = true;
                if !match *e {
                    Exp::And(box ref mut f, box ref mut s)
                    | Exp::Or(box ref mut f, box ref mut s)
                    | Exp::Xor(box ref mut f, box ref mut s)
                    | Exp::Imp(box ref mut f, box ref mut s)
                    | Exp::Nand(box ref mut f, box ref mut s)
                    | Exp::Iff(box ref mut f, box ref mut s)
                    => {
                        Prop::minimal(f) || Prop::minimal(s)
                    },
                    Exp::Not(box ref mut f) => Prop::minimal(f),
                    _ => { false },
                }
                {break;}
            }
            // {}
            
            b
        }
        
        pub fn nand_normal_form(&mut self) {
            let mut x = Exp::Empty;
            swap(&mut x, &mut self.root);
            Prop::nander(&mut x);
            swap(&mut x, &mut self.root);
        }
        
        // works on minimized formulas only
        fn nander(e: &mut Exp) {
            use ::parser::Exp::{Iff, Imp, Xor, Or, And, Nand, Not, Var, Empty};
            let r = e;
            
            let _ = match *r {
                Var(_) | Exp::True | Exp::False => {},
                And(box ref mut f, box ref mut s)
                | Or(box ref mut f, box ref mut s)
                    => { Prop::nander(f); Prop::nander(s); },
                Not(box ref mut l) => { Prop::nander(l); },
                _ => {},
            };
            
            let t =
            match *r {
                And(box ref mut f, box ref mut s)
                => {
                    Nand( box Nand(box f.clone(), box s.clone()), box Nand(box f.clone(), box s.clone()) )
                },
                Or(box ref mut f, box ref mut s)
                => {
                    Nand( box Nand(box f.clone(), box f.clone()), box Nand(box s.clone(), box s.clone()) )
                },
                Not(box ref mut f)
                => {
                    Nand( box f.clone(), box f.clone() )
                },
                ref r => r.clone(),
            };
            
            *r = t;
        }
        
        pub fn linearize(&mut self) {
            let mut x = Exp::Empty;
            swap(&mut x, &mut self.root);
            Prop::linear(&mut x);
            swap(&mut x, &mut self.root);
        }
        
        fn linear(e: &mut Exp) -> bool {
            let mut r = match *e {
                Exp::Var(..) | Exp::Vari(..) | Exp::True | Exp::False | Exp::Empty => Exp::Empty,
                Exp::And(box ref mut a, box ref mut b)
                => {
                    if let Exp::And_(ref mut v) = *a {
                        Prop::linear(b);
                        if let Exp::And_(ref mut v2) = *b {
                            v.append(v2);
                        }
                        else {
                            v.push(b.clone());
                        }
                        let mut x = vec![];
                        swap(&mut x, v);
                        Exp::And_(x)
                    }
                    else
                    if let Exp::And_(ref mut v) = *b {
                        Prop::linear(a);
                        if let Exp::And_(ref mut v2) = *a {
                            v.append(v2);
                        }
                        else {
                            v.push(a.clone());
                        }
                        let mut x = vec![];
                        swap(&mut x, v);
                        Exp::And_(x)
                    }
                    else {
                        Prop::linear(a);
                        Prop::linear(b);
                        let mut v = vec![a.clone(), b.clone()];
                        Exp::And_(v)
                    }
                },
                Exp::Or(box ref mut a, box ref mut b)
                => {
                    if let Exp::Or_(ref mut v) = *a {
                        Prop::linear(b);
                        if let Exp::Or_(ref mut v2) = *b {
                            v.append(v2);
                        }
                        else {
                            v.push(b.clone());
                        }
                        let mut x = vec![];
                        swap(&mut x, v);
                        Exp::Or_(x)
                    }
                    else
                    if let Exp::Or_(ref mut v) = *b {
                        Prop::linear(a);
                        if let Exp::Or_(ref mut v2) = *a {
                            v.append(v2);
                        }
                        else {
                            v.push(a.clone());
                        }
                        let mut x = vec![];
                        swap(&mut x, v);
                        Exp::Or_(x)
                    }
                    else {
                        Prop::linear(a);
                        Prop::linear(b);
                        let mut v = vec![a.clone(), b.clone()];
                        Exp::Or_(v)
                    }
                },
                Exp::Xor(box ref mut a, box ref mut b)
                => {
                    if let Exp::Xor_(ref mut v) = *a {
                        Prop::linear(b);
                        if let Exp::Xor_(ref mut v2) = *b {
                            v.append(v2);
                        }
                        else {
                            v.push(b.clone());
                        }
                        let mut x = vec![];
                        swap(&mut x, v);
                        Exp::Xor_(x)
                    }
                    else
                    if let Exp::Xor_(ref mut v) = *b {
                        Prop::linear(a);
                        if let Exp::Xor_(ref mut v2) = *a {
                            v.append(v2);
                        }
                        else {
                            v.push(a.clone());
                        }
                        let mut x = vec![];
                        swap(&mut x, v);
                        Exp::Xor_(x)
                    }
                    else {
                        Prop::linear(a);
                        Prop::linear(b);
                        let mut v = vec![a.clone(), b.clone()];
                        Exp::Xor_(v)
                    }
                },
                _ => Exp::Empty,
            };
            
            let b = if r != Exp::Empty {
                swap(&mut r, e);
                true
            }
            else { false };
            
            while match *e {
                Exp::And_(ref mut va)
                => {
                    let mut b = false;
                    for ref mut a in va {
                        b |= Prop::linear(a);
                    }
                    
                    b
                },
                Exp::Or_(ref mut va)
                => {
                    let mut b = false;
                    for ref mut a in va {
                        b |= Prop::linear(a);
                    }
                    
                    b
                },
                Exp::Xor_(ref mut va)
                => {
                    let mut b = false;
                    for ref mut a in va {
                        b |= Prop::linear(a);
                    }
                    
                    b
                },
                _ => false,
            }
            {}
            
            b
        }
        
        fn print_exp(prec: i32, e: &Exp) -> String {
            let print_exp = Prop::print_exp;
            use ::parser::Exp::{Iff, Imp, Xor, Or, And, Nand, Not, Var, Empty};
            let my_tag = tag(e);
            // use a dictionary for unicode/ascii style swithces
            let mut prec2 = 6;
            let s = match *e {
                Iff(ref f, ref s) => { prec2 = 0; format!("({} ⇔ {})", print_exp(0, f), print_exp(0, s)) },
                Imp(ref f, ref s) => { prec2 = 1; format!("({} ⇒ {})", print_exp(1, f), print_exp(1, s)) },
                Xor(ref f, ref s) => { prec2 = 2; format!("({} ⊕ {})", print_exp(2, f), print_exp(2, s)) },
                Or(ref f, ref s) => { prec2 = 3; format!("({} ∨ {})", print_exp(3, f), print_exp(3, s)) },
                // And(ref f, ref s) => { prec2 = 3; format!("({} ∧ {})", print_exp(3, f), print_exp(3, s)) },
                And(ref f, ref s) => { prec2 = 4; format!("({} ⋅ {})", print_exp(4, f), print_exp(4, s)) },
                Nand(ref f, ref s) => { prec2 = 4; format!("({} ↑ {})", print_exp(4, f), print_exp(4, s)) },
                Not(ref f) => { prec2 = 5; format!("(¬{})", print_exp(5, f)) },
                Var(ref f) => format!("({})", f.clone()),
                Exp::True => "[1]".to_owned(),
                Exp::False => "[0]".to_owned(),
                Exp::And_(ref v) | Exp::Xor_(ref v) | Exp::Or_(ref v)
                => {
                    let mut s = "(".to_owned();
                    let (op, pr) = {
                        if my_tag == tag(&Exp::And_(vec![]))
                            { ("∧", 4) }
                        else if my_tag == tag(&Exp::Or_(vec![]))
                            { ("∨", 3) }
                        else
                            { ("⊕", 2) }
                    }.to_owned();
                    prec2 = pr;
                    for ref a in v[0..v.len()-1].iter() {
                        let s2 = print_exp(pr, a);
                        let s2 = s2.as_str();
                        s.push_str( [s2, " ", op, " "].concat().as_str() );
                    }
                    let a = &v[v.len()-1];
                    let s2 = print_exp(pr, a);
                    s.push_str( (s2 + ")").as_str() );
                    s
                },
                Empty => "( )".to_owned(),
                _ => "".to_owned(),
            };
            if prec2 >= prec && e.associative() {
                s[1..s.len()-1].to_owned()
            }
            else {
                s
            }
        }
        
        pub fn print(&self) {
            println!("```{}```", Prop::print_exp(0, &self.root));
        }
        
        pub fn build(&mut self) {
            // assert_eq!(self.root, Exp::Empty);
            let mut x = self.root.clone();
            self.s(&mut x);
            self.root = x;
        }
        
        /// FIXME: Doesn't handle chaining well (at all).
        fn s(&mut self, place: &mut Exp) -> bool {
            // println!("s|{:?}| {:?}", self.toks.next(), place);
            let mut pp = place.clone();
            
            if !self.e(&mut pp) {
                false
            }
            else {
                if let Some(Token::Op(Op::Iff)) = self.toks.next() {
                    // println!("[[{:?}][{:?}]]", pp, *place);
                    self.toks.consume();
                    *place = Exp::Iff(box pp, box Exp::Empty);
                    if let Exp::Iff(_, box ref mut pp) = *place {
                        self.e(pp)
                    }
                    else { false }
                }
                else {
                    // println!("[[{:?}][{:?}]]", pp, *place);
                    *place = pp;
                    self.toks.next() == None
                }
            }
        }
        
        fn p(&mut self, place: &mut Exp) -> bool {
            // println!("p|{:?}| {:?}", self.toks.next(), place);
            if let Some(Token::PLeft) = self.toks.next() {
                self.toks.consume();
                let b = self.e(place) || self.s(place);
                b && self.toks.expect(Token::PRight)
            }
            else {
                false
            }
        }
        
        fn e(&mut self, place: &mut Exp) -> bool {
            // println!("e|{:?}| {:?}", self.toks.next(), place);
            if !self.e2(place) {
                return false;
            }
            
            while let Some(Token::Op(Op::Imp)) = self.toks.next() {
                self.toks.consume();
                let pp = box place.clone();
                *place = Exp::Imp(pp, box Exp::Empty);
                if let Exp::Imp(_, box ref mut ppp) = *place {
                    return self.e2(ppp);
                }
            }
            
            self.toks.expect(Token::Empty)
        }
        
        fn e2(&mut self, place: &mut Exp) -> bool {
            // println!("e2|{:?}| {:?}", self.toks.next(), place);
            if !self.e3(place) {
                return false;
            }
            
            while let Some(Token::Op(Op::Xor)) = self.toks.next() {
                self.toks.consume();
                let pp = box place.clone();
                *place = Exp::Xor(pp, box Exp::Empty);
                if let Exp::Xor(_, box ref mut ppp) = *place {
                    return self.e3(ppp);
                }
            }
            
            true
        }
        
        fn e3(&mut self, place: &mut Exp) -> bool {
            // println!("e3|{:?}| {:?}", self.toks.next(), place);
            if !self.e4(place) {
                return false;
            }
            
            while let Some(Token::Op(Op::Or)) = self.toks.next() {
                self.toks.consume();
                let pp = box place.clone();
                *place = Exp::Or(pp, box Exp::Empty);
                if let Exp::Or(_, box ref mut ppp) = *place {
                    if !self.e4(ppp) {
                        return false;
                    }
                }
            }
            
            true
        }
        
        fn e4(&mut self, place: &mut Exp) -> bool {
            // println!("e4|{:?}| {:?}", self.toks.next(), place);
            if !self.e5(place) {
                return false;
            }
            
            while let Some(Token::Op(Op::And)) = self.toks.next() {
                self.toks.consume();
                let pp = box place.clone();
                *place = Exp::And(pp, box Exp::Empty);
                if let Exp::And(_, box ref mut ppp) = *place {
                    if !self.e5(ppp) {
                        return false;
                    }
                }
            }
            
            true
        }
        
        fn e5(&mut self, place: &mut Exp) -> bool {
            // println!("e5|{:?}| {:?}", self.toks.next(), place);
            if let Some(Token::Op(Op::Not)) = self.toks.next() {
                while let Some(Token::Op(Op::Not)) = self.toks.next() { self.toks.consume(); }
                *place = Exp::Not(box Exp::Empty);
            
                if let Exp::Not(box ref mut x) = *place {
                    self.var0(x)
                }
                else {
                    unreachable!()
                }
            }
            else {
                self.var0(place)
            }
        }
        
        fn var0(&mut self, place: &mut Exp) -> bool {
            // println!("e6|{:?}| {:?}", self.toks.next(), place);
            self.var(place) || self.p(place)
        }
        
        fn var(&mut self, place: &mut Exp) -> bool {
            // println!("var|{:?}| {:?}", self.toks.next(), place);
            if let Some(Token::Var(x)) = self.toks.next() {
                self.toks.consume();
                *place = Exp::Var(x.clone());
                true
            }
            else {
                false
            }
        }
    }
}

mod t {
    pub use ::parser::*;
}

fn main() {
    let input = vec![
                    // "y & k & l",
                    // "x & y & y & j & j || u || l || y && (t || l)",
                    // "~(~u)",
                    // "~~~~~h",
                    "~x & (!u)",
                    // "(x && (bar[x] & foo || d[u]) >< ~((~t) || f & s)) <=> T",
                    // "x && (bar[x] & foo || d[u]) >< ~((~t) || f & s) <=> T",
                    "x && ~~(bar[x] & foo || d[u]) >< ~(~(~t) || f & s)",
                    "x || (y & !z)"
                    ];
    
    for (i, s) in input.iter().enumerate() {
        let mut tree = t::Stmt::new();
        println!("\n=====\nIn[{}]:= \"{}\"", i, s);
        
        tree.tokenize(&s);
        // println!("{:?}", &tree);
        
        let mut p = t::Prop::from_tokens(&tree);
        
        println!("Out[{}]:= ", i);
        p.print();
        p.minimal_set_form();
        p.minimize();
        let mut p2 = p.clone();
        p.linearize();
        p.minimize();
        println!("\nOut[min]:= ");
        p.print();
        // println!("\nOut[ANF]:= ");
        // p.algebraic_normal_form();
        // p.minimize();
        // p.print();
        // p2.minimal_set_form();
        // p2.minimize();
        println!("\nOut[NAND]:= ");
        p2.nand_normal_form();
        p2.print();
        // p = t::Prop::from_tokens(&tree);
        // p.minimal_set_form();
        // p.linearize();
        // println!("Out[linearized]:= ");
        // p.print();
    }
}


















