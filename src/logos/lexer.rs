#![allow(
    unused_imports,
    dead_code,
    unused_assignments,
    unused_mut,
    unused_variables,
    deprecated,
    non_snake_case,
)]

#![feature(
    type_ascription,
    box_syntax,
    box_patterns,
    inclusive_range_syntax,
    core_intrinsics,
    pub_restricted,
    slice_patterns,
    // range_contains,
    // str_char,
)]

use std::collections::*;
use std::convert::*;
use std::char::*;
use std::option::*;
use std::mem::*;
use std::mem;
use std::cmp::{max, min};
use std::intrinsics;
use std::cmp;
use std::ops;
use std;

// pub mod mtoken {
#[derive(Debug, Clone, PartialEq,)]
pub enum Token {
    Empty,
    T,
    F,
    Lp,
    Rp,
    And,
    Or,
    Xor,
    Not,
    Nand,
    Nor,
    Imp,
    Iff,
    P(String),
    N(i32),
}

impl std::default::Default for Token {
    fn default() -> Token { Token::Empty }
}

impl ::logos::lexer::Token {
    pub fn tokenize(s: &str) -> Option<Token> {
        let is_p = |s: &str| {
            s.chars().all( char::is_alphabetic )
        };
        let is_num = |s: &str| {
            s.chars().all( char::is_numeric )
        };
        match s {
            "&" | "&&" => Some(Token::And),
            "+" | "><" | ">-<" | "!=" | "/="
            => Some(Token::Xor),
            "!" | "~" => Some(Token::Not),
            "||" => Some(Token::Or),
            "|" | "~&" => Some(Token::Nand),
            "~||" => Some(Token::Nor),
            "<=>" | "<->" => Some(Token::Iff),
            "=>" | "->" => Some(Token::Imp),
            "(" => Some(Token::Lp),
            ")" => Some(Token::Rp),

            a if is_p(a) => Some(Token::P(a.into())),
            a if is_num(a) => Some(Token::N(a.parse::<i32>().unwrap())),

            _ => None,
        }
    }
}

/// A string of `Token`s usable by proposition sentence builders like `::logos::Prop`
#[derive(Debug, Clone, PartialEq, Default,)]
pub struct TokenStr {
    tokens: Vec<Token>,
    ptr: usize,
    len: usize,
}

impl TokenStr {
    // build a `Token` string from a `char` string.
    pub fn from_str(mut text: String) -> TokenStr {
        let mut tokens = vec![Token::Empty];
        text = text.chars()
                    .filter(
                        |c| !char::is_whitespace(*c)
                    )
                    .collect()
                    ;

        let mut i = 0;
        // for the whole string
        while i < text.len() {
            let mut j = text.len();
            // find the longest possible subtring starting from i that can be tokenized
            // FIXME inefficient.
            while let None = Token::tokenize(&text[i..j]) {
                if j > i + 1 {
                    j -= 1;
                }
                else { break; }
            }
            // once found, push new token and advance index to 
            if let Some(tok) = Token::tokenize(&text[i..j]) {
                tokens.push(tok);
            }
            i = j;
        }
        let len = tokens.len();
        TokenStr { len: len, tokens: tokens, ptr: 0 }
    }
    pub fn current(&self) -> Option<Token> {
        if self.ptr < self.len {
            Some(self.tokens[self.ptr].clone())
        }
        else { None }
    }
    pub fn next(&self) -> Option<Token> {
        if self.ptr + 1 < self.len {
            Some(self.tokens[self.ptr + 1].clone())
        }
        else { None }
    }
    pub fn pop(&mut self) -> bool {
        if self.ptr < self.len {
            self.ptr += 1;
            true
        }
        else { false }
    }
    pub fn popN(&mut self, i: usize) -> bool {
        if self.ptr + i < self.len {
            self.ptr += i;
            true
        }
        else { false }
    }
    pub fn expect(&mut self, tok: Token) -> bool {
        if let Some(t) = self.next() {
            if t == tok {
                self.pop();
                return true;
            }
        }
        false
    }
    pub fn reset(&mut self) { self.ptr = 0; }
    pub fn ptr(&self) -> usize { self.ptr }
    pub fn len(&self) -> usize { self.len }
    pub fn done(&self) -> bool { self.ptr == self.len }
}
// }