#![allow(
    unused_imports,
    dead_code,
    unused_assignments,
    unused_mut,
    unused_variables,
    deprecated,
    non_snake_case,
)]

#![feature(
    type_ascription,
    box_syntax,
    box_patterns,
    inclusive_range_syntax,
    core_intrinsics,
    pub_restricted,
    slice_patterns,
    // range_contains,
    // str_char,
)]

use std::collections::*;
use std::convert::*;
use std::char::*;
use std::option::*;
use std::mem::*;
use std::mem;
use std::cmp::{max, min};
use std::intrinsics;
use std::cmp;
use std::ops;

// mod logos {
    // pub use std::mem::swap;
    // use std::vec::*;
use std::boxed::*;

pub fn flash<T: ::std::default::Default>(l: &mut T) -> T {
    let mut r = T::default();
    swap(l, &mut r);
    r
}

mod utils {
    // NAND is !all_and
    pub fn all_and(bs: &[bool]) -> bool {
        bs.iter().fold( true, |prev, b| prev && *b )
    }

    // NOR is !all_or
    pub fn all_or(bs: &[bool]) -> bool {
        bs.iter().fold( false, |prev, b| prev || *b )
    }

    // XNOR is !all_xor
    pub fn all_xor(bs: &[bool]) -> bool {
        bs.iter().fold( false, |prev, b| prev ^ b )
    }
}

pub mod lexer;
pub mod parser;

use self::parser::Exp;
use self::lexer::TokenStr;

#[derive(Debug, Clone, PartialEq, Default,)]
pub struct Prop {
    exp: Exp
}

impl ::std::fmt::Display for Prop {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> Result<(), ::std::fmt::Error> {
        write!(f, "{}", self.to_string())
    }
}


/// construction and display
impl Prop {
    /// Construct a `Prop` from a valid `TokenStr`
    pub fn from_sentence(mut sen: TokenStr) -> Prop {
        let exp = Exp::from_token_str(sen);
        Prop { exp: exp }
    }

    /// Get string representation of current `Exp` tree
    /// with Unicode symbols used for some
    pub fn to_string(&self) -> String {
        self.exp.to_string()
    }
}

impl Prop {
    pub fn apply_with<F>(&self, f: F) -> Exp
        where F: Fn(&str, i32) -> Option<bool>
    {
        let p = Prop::_reduce(&self.exp, &f);
        // run `quine_mccluskey` on this before returning, once qm is finished.
        p
    }
    
    fn _reduce<F>(e: &Exp, f: &F) -> Exp
        where F: Fn(&str, i32) -> Option<bool>
    {
        let reduce = Prop::_reduce;
        match *e {
            Exp::And(box ref a, box ref b) => {
                    let a = reduce(a, f);
                    let b = reduce(b, f);
                    if let(Some(a), Some(b)) = (a.is_bool(), b.is_bool()) {
                        if a && b { Exp::T } else { Exp::F }
                    }
                    else {
                        Exp::And(box a, box b)
                    }
                },
            Exp::Or(box ref a, box ref b) => {
                    let a = reduce(a, f);
                    let b = reduce(b, f);
                    if let(Some(a), Some(b)) = (a.is_bool(), b.is_bool()) {
                        if a || b { Exp::T } else { Exp::F }
                    }
                    else {
                        Exp::Or(box a, box b)
                    }
                },
            Exp::Xor(box ref a, box ref b) => {
                    let a = reduce(a, f);
                    let b = reduce(b, f);
                    if let(Some(a), Some(b)) = (a.is_bool(), b.is_bool()) {
                        if a ^ b { Exp::T } else { Exp::F }
                    }
                    else {
                        Exp::Xor(box a, box b)
                    }
                },
            Exp::Iff(box ref a, box ref b) => {
                    let a = reduce(a, f);
                    let b = reduce(b, f);
                    if let(Some(a), Some(b)) = (a.is_bool(), b.is_bool()) {
                        if (a && b) || (!a && !b) { Exp::T } else { Exp::F }
                    }
                    else {
                        Exp::Iff(box a, box b)
                    }
                },
            Exp::Imp(box ref a, box ref b) => {
                    let a = reduce(a, f);
                    let b = reduce(b, f);
                    if let(Some(a), Some(b)) = (a.is_bool(), b.is_bool()) {
                        if !a || b { Exp::T } else { Exp::F }
                    }
                    else {
                        Exp::Imp(box a, box b)
                    }
                },
            Exp::Nand(box ref a, box ref b) => {
                    let a = reduce(a, f);
                    let b = reduce(b, f);
                    if let(Some(a), Some(b)) = (a.is_bool(), b.is_bool()) {
                        if !(a && b) { Exp::T } else { Exp::F }
                    }
                    else {
                        Exp::Nand(box a, box b)
                    }
                },
            Exp::Nor(box ref a, box ref b) => {
                    let a = reduce(a, f);
                    let b = reduce(b, f);
                    if let(Some(a), Some(b)) = (a.is_bool(), b.is_bool()) {
                        if !(a || b) { Exp::T } else { Exp::F }
                    }
                    else {
                        Exp::Nor(box a, box b)
                    }
                },
            Exp::Not(box ref n) => {
                    let n = reduce(n, f);
                    if let Some(n) = n.is_bool() {
                        if n { Exp::F } else { Exp::T }
                    }
                    else {
                        Exp::Not(box n)
                    }
                },
            Exp::P(ref p) => {
                    if let Some(b) = f(p, 0) {
                        if b { Exp::T } else { Exp::F }
                    }
                    else { e.clone() }
                },
            Exp::Pv(ref p, i) => {
                    if let Some(b) = f(p, i) {
                        if b { Exp::T } else { Exp::F }
                    }
                    else { e.clone() }
                },
            Exp::T | Exp::F => e.clone(),
            Exp::Empty => Exp::Empty,
        }
    }
    
    pub fn eval_with<F>(&self, func: F) -> bool
        where F: Fn(&str, i32) -> bool
    {
        Prop::_eval(&self.exp, &func)
    }
    
    fn _eval<F>(e: &Exp, func: &F) -> bool
        where F: Fn(&str, i32) -> bool
    {
        let _eval = Prop::_eval;
        match *e {
            Exp::And(box ref a, box ref b)
                => _eval(a, func) && _eval(b, func),
            Exp::Or(box ref a, box ref b)
                => _eval(a, func) || _eval(b, func),
            Exp::Xor(box ref a, box ref b)
                => _eval(a, func) ^ _eval(b, func),
            Exp::Iff(box ref a, box ref b)
                => {
                    let x = _eval(a, func);
                    let y = _eval(b, func);
                    ((x && y) || (!x && !y))
                },
            Exp::Imp(box ref a, box ref b)
                => !_eval(a, func) || _eval(b, func),
            Exp::Nand(box ref a, box ref b)
                => !(_eval(a, func) && _eval(b, func)),
            Exp::Nor(box ref a, box ref b)
                => !(_eval(a, func) || _eval(b, func)),
            Exp::Not(box ref n) => !_eval(n, func),
            Exp::P(ref p) => func(p, 0),
            Exp::Pv(ref p, i) => func(p, i),
            Exp::T => true,
            Exp::F => false,
            Exp::Empty => panic!("Invalid operation in evaluation tree."),
        }
    }
}

/// Algebraic Normal Form construction
impl Prop {
    /// Get equivalent proposition with only `{Xor, And, Not}` operations used
    pub fn algebraic_normal_form(mut self) -> Prop {
        let _ = Prop::_to_anf(&mut self.exp);
        self
    }

    // recursive passes
    fn _to_anf_recs(e: &mut Exp) -> bool {
        use self::parser::Exp::{Empty, Iff, Imp, Xor, T, F, And, Or, Not, Nand, Nor, P, Pv};
        match *e {
            Empty | T | F | P(..) | Pv(..)  => false,
            And(ref mut x, ref mut y)
            | Or(ref mut x, ref mut y)
            | Xor(ref mut x, ref mut y)
            => { Prop::_to_anf(x) || Prop::_to_anf(y) },
            Not(box ref mut n)
            => { Prop::_to_anf(n) },
            _ => false,
        }
    }

    // transformation rules
    fn _to_anf(e: &mut Exp) -> bool {
        use self::parser::Exp::{Empty, Iff, Imp, Xor, T, F, And, Or, Not, Nand, Nor, P, Pv};
        Prop::_to_anf_recs(e);

        let e2 = match *e {
            // optimize for F & x cases [DONE]
            And(box ref mut x, box ref mut y)
            => {
                if *x == T {
                    flash(y)
                }
                else
                  if *y == T {
                    flash(x)
                }
                else
                  if *x == F || *y == F {
                    F
                }
                else
                  if let Xor(ref mut u, ref mut v) = *x {
                    Xor(box And(box flash(u), box y.clone()), box And(box flash(v), box y.clone()))
                }
                else
                  if let Xor(ref mut u, ref mut v) = *y {
                    Xor(box And(box x.clone(), box flash(u)), box And(box x.clone(), box flash(v)))
                }
                else {
                    Empty
                } 
            },
            Not(/*box ref mut n*/..)
            => {
                // let x = flash(n);
                // Exp::Xor(box T, box x)
                Empty
            }
            Or(box ref x, box ref y)
            // optimize for (T || x) and (F || x) cases
            => Xor(
                box Xor(box x.clone(), box y.clone()),
                box And(box x.clone(), box y.clone())
                ),
            _ => Empty
        };

        if e2 != Empty {
            *e = e2;
        }

        Prop::_to_anf_recs(e)
    }
}


/// Minimal Set Form {AND, OR, NOT}
impl Prop {
    /// Get equivalent `Prop` with only `{And, Or, Not}`
    /// operations used
    pub fn minimal_set_form(mut self) -> Prop {
        Prop::_to_msf(&mut self.exp);
        self
    }

    // recursive check calls for the transformation function `fn _to_msf`
    fn _to_msf_recs(e: &mut Exp) {
        use self::parser::Exp::{Empty, Iff, Imp, Xor, T, F, And, Or, Not, Nand, Nor, P, Pv};
        match *e {
            Empty | T | F | P(..) | Pv(..)
            => {},
            Iff(box ref mut x, box ref mut y)
            | Imp(box ref mut x, box ref mut y)
            | Xor(box ref mut x, box ref mut y)
            | Or(box ref mut x, box ref mut y)
            | And(box ref mut x, box ref mut y)
            | Nand(box ref mut x, box ref mut y)
            | Nor(box ref mut x, box ref mut y)
            => {
                Prop::_to_msf(x);
                Prop::_to_msf(y);
            },
            Not(box ref mut x) => { Prop::_to_msf(x); },
        }
    }

    // transformation rules for propositions to minimal_set_form
    fn _to_msf(e: &mut Exp) {
        use self::parser::Exp::{Empty, Iff, Imp, Xor, T, F, And, Or, Not, Nand, Nor, P, Pv};
        
        Prop::_to_msf_recs(e);
        
        let e2 = match *e {
            // (A xor B) == (A & !B) || (!A & B)
            Xor(box ref x, box ref y)
            => Or(
                box And(box x.clone(), box Not(box y.clone())),
                box And(box Not(box x.clone()), box y.clone())
                ),
            // A <=> B == (A & B) || (!A & !B)
            Iff(box ref x, box ref y)
            => Or(
                box And(box x.clone(), box y.clone()),
                box And(box Not(box x.clone()), box Not(box y.clone()))
                ),
            // A => B == !A || B
            Imp(box ref x, box ref y)
            => Or( box Not(box x.clone()), box y.clone() ),
            // !!A == A
            Not(box ref mut n)
            => {
                if let Not(box ref mut x) = *n {
                    flash(x)
                }
                else { Empty }
            }
            // Nand(box ref x, box ref y) => ,
            // Nor(box ref x, box ref y) => ,
            _ => Empty,
        };
        
        if e2 != Empty {
            *e = e2;
        }

        Prop::_to_msf_recs(e);
    }
}


/// NAND form
impl Prop {
    /// Get equivalent `Prop` with only `{Nand}` operation used
    pub fn nand_normal_form(mut self) -> Prop {
        Prop::_to_nnf(&mut self.exp);
        self
    }

    // NAND form transformation rules
    fn _to_nnf(e: &mut Exp) {
        use self::parser::Exp::{Empty, Iff, Imp, Xor, T, F, And, Or, Not, Nand, Nor, P, Pv};
        // recursive application of transformation rules
        // breaks recursion on reaching an atom ( `{T, F, P, Pv, Empty}` )
        match *e {
            ref a if a.is_atom() => {},
            And(ref mut x, ref mut y)
            | Or(ref mut x, ref mut y)
            | Nand(ref mut x, ref mut y)
            => { Prop::_to_nnf(x); Prop::_to_nnf(y); },
            Not(box ref mut n)
            => { Prop::_to_nnf(n); },
            _ => {}
        }
        // transformation rules to NAND-only form
        // `~&` === `NAND`
        let e2 = match *e {
            // A & B == ( A ~& B ) ~& (A ~& B)
            And(box ref mut x, box ref mut y)
            => Nand(
                box Nand(box x.clone(), box y.clone()),
                box Nand(box x.clone(), box y.clone())
                ),
            // A || B == (A ~& A) ~& (B ~& B)
            Or(box ref mut x, box ref mut y)
            => Nand(
                box Nand(box x.clone(), box x.clone()),
                box Nand(box y.clone(), box y.clone())
                ),
            // !A == A ~& A
            Not(box ref mut n)
            => Nand(box n.clone(), box n.clone()),
            _ => Empty,
        };

        if e2 != Empty {
            *e = e2;
        }
    }
}

/// experimental
impl Prop {
    
    /// Get optimum equivalent proposition
    pub fn quine_mccluskey(mut p: Prop) -> Prop {
        Prop::_qs_mini(&mut p.exp);
        p
    }

    // quine mccluskey minimization
    fn _qs_mini(e: &mut Exp) {
        // make a table and start minimizing;
    }
}

// } mod logos