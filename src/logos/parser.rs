#![allow(
    unused_imports,
    dead_code,
    unused_assignments,
    unused_mut,
    unused_variables,
    deprecated,
    non_snake_case,
)]

#![feature(
    type_ascription,
    box_syntax,
    box_patterns,
    inclusive_range_syntax,
    core_intrinsics,
    pub_restricted,
    slice_patterns,
    // range_contains,
    // str_char,
)]

use std::collections::*;
use std::convert::*;
use std::char::*;
use std::option::*;
use std::mem::*;
use std::mem;
use std::cmp::{max, min};
use std::intrinsics;
use std::cmp;
use std::ops;
use std;

// pub mod parser {
use super::lexer::TokenStr;
// use ::std::mem::swap;
use super::flash;

/// Expression type
/// represented as a unique tree of operators and atoms
/// Atoms are: `{ T, F, P, Pv, Empty }`
#[derive(Debug, Clone, PartialEq,)]
pub enum Exp {
    Empty,
    T, F,
    Imp(Box<Exp>, Box<Exp>),
    Iff(Box<Exp>, Box<Exp>),
    Xor(Box<Exp>, Box<Exp>),
    Or(Box<Exp>, Box<Exp>),
    And(Box<Exp>, Box<Exp>),
    Not(Box<Exp>),
    Nand(Box<Exp>, Box<Exp>),
    Nor(Box<Exp>, Box<Exp>),
    /// Propositional variable
    P(String),
    /// Propositional function applied to an argument
    Pv(String, i32),
}

impl Exp {
    pub fn is_bool(&self) -> Option<bool> {
        match *self {
            Exp::T => Some(true),
            Exp::F => Some(false),
            _ => None
        }
    }
    
    pub fn associative(&self) -> bool {
        match *self {
            Exp::Nand(..) | Exp::Nor(..) => false,
            _ => true
        }
    }
    fn precedence(&self) -> u32 {
        match *self {
            Exp::Iff(..) => 6,
            Exp::Imp(..) => 5,
            Exp::Or(..) | Exp::Nor(..) => 3,
            Exp::And(..) | Exp::Nand(..) => 3,
            Exp::Xor(..) => 2,
            Exp::Not(..) => 1,
            Exp::P(..) | Exp::Pv(..) | Exp::T | Exp::F => 0,
            _ => 1000,
        }
    }
    pub fn is_atom(&self) -> bool {
        use self::Exp::*;
        match *self {
            Empty | T | F | P(..) | Pv(..)  => true,
            _ => false,
        }
    }
    pub fn to_string(&self) -> String {
        self._to_s(7)
    }
    pub fn _to_s(&self, p: u32) -> String {
        use self::Exp::{Iff, Imp, Xor, T, F, And, Or, Not, Nand, Nor, P, Pv};
        let p2 = self.precedence();
        let s = match *self {
            // Iff(ref x, ref y) => format!("{} ⇔ {}", x._to_s(p2), y._to_s(p2)),
            Iff(ref x, ref y) => format!("{} <=> {}", x._to_s(p2), y._to_s(p2)),
            // Imp(ref x, ref y) => format!("{} ⇒ {}", x._to_s(p2), y._to_s(p2)),
            Imp(ref x, ref y) => format!("{} => {}", x._to_s(p2), y._to_s(p2)),
            // Xor(ref x, ref y) => format!("{} ⊕ {}", x._to_s(p2), y._to_s(p2)),
            // Xor(ref x, ref y) => format!("{} =/= {}", x._to_s(p2), y._to_s(p2)),
            Xor(ref x, ref y) => format!("{} >< {}", x._to_s(p2), y._to_s(p2)),
            // Or(ref x, ref y) => format!("{} ∨ {}", x._to_s(p2), y._to_s(p2)),
            Or(ref x, ref y) => format!("{} || {}", x._to_s(p2), y._to_s(p2)),
            // And(ref x, ref y) => format!("{} ⋅ {}", x._to_s(p2), y._to_s(p2)),
            And(ref x, ref y) => format!("{} & {}", x._to_s(p2), y._to_s(p2)),
            // Nand(ref x, ref y) => format!("{} ↑ {}", x._to_s(p2), y._to_s(p2)),
            Nand(ref x, ref y) => format!("{} | {}", x._to_s(p2), y._to_s(p2)),
            // Nor(ref x, ref y) => format!("{} ↓ {}", x._to_s(p2), y._to_s(p2)),
            Nor(ref x, ref y) => format!("{} ~|| {}", x._to_s(p2), y._to_s(p2)),
            Not(ref x) => format!("¬{}", x._to_s(p2)),
            T => "1".to_owned(),
            F => "0".to_owned(),
            P(ref s) => format!("{}", s),
            Pv(ref s, i) => format!("{}({})", s, i),
            Exp::Empty => "#".to_owned(),
        };

        if p2 > p || !self.associative() {
            format!("({})", s)
        }
        else {
            s
        }
    }
    pub fn from_token_str(mut sen: TokenStr) -> Exp {
        let mut exp = Exp::Empty;
        grammar::start(&mut sen, &mut exp);
        exp
    }
}

impl std::default::Default for Exp {
    fn default() -> Self { Exp::Empty }
}

impl std::fmt::Display for Exp {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{}", self.to_string())
    }
}

mod grammar {
    use super::{ Exp, };
    use logos::lexer::{ TokenStr, Token };
    use logos::flash;
    use ::std::mem::swap;
    // iff -> imp -> parens -> xor -> or -> and -> not -> subexp -> atom
    pub fn start(sen: &mut TokenStr, e: &mut Exp) -> bool {
        iff(sen, e)
    }
    pub fn iff(sen: &mut TokenStr, e: &mut Exp) -> bool {
        if !imp(sen, e) { return false; }

        if let Some(Token::Iff) = sen.next() {
            sen.pop();
            let x = flash(e);
            *e = Exp::Iff(box x, box Exp::Empty);
            if let Exp::Iff(_, box ref mut y) = *e {
                return imp(sen, y);
            }
        }
        sen.done()
    }

    pub fn parens(sen: &mut TokenStr, e: &mut Exp) -> bool {
        (sen.expect(Token::Lp)
            && (xor(sen, e) || iff(sen, e))
            && sen.expect(Token::Rp))
    }

    pub fn imp(sen: &mut TokenStr, e: &mut Exp) -> bool {
        if !xor(sen, e) { return false; }

        if let Some(Token::Imp) = sen.next() {
            sen.pop();
            let x = flash(e);
            *e = Exp::Imp(box x, box Exp::Empty);
            if let Exp::Imp(_, box ref mut y) = *e {
                return xor(sen, y);
            }
        }
        true
    }

    pub fn xor(sen: &mut TokenStr, e: &mut Exp) -> bool {
        if !or(sen, e) { return false; }

        while let Some(Token::Xor) = sen.next() {
            sen.pop();
            let x = flash(e);
            *e = Exp::Xor(box x, box Exp::Empty);
            if let Exp::Xor(_, box ref mut y) = *e {
                if !or(sen, y) { return false; }
            }
        }
        true
    }

    pub fn or(sen: &mut TokenStr, e: &mut Exp) -> bool {
        if !and(sen, e) { return false; }

        while let Some(Token::Or) = sen.next() {
            sen.pop();
            let x = flash(e);
            *e = Exp::Or(box x, box Exp::Empty);
            if let Exp::Or(_, box ref mut y) = *e {
                if !and(sen, y) { return false; }
            }
        }
        true
    }

    pub fn and(sen: &mut TokenStr, e: &mut Exp) -> bool {
        if !not(sen, e) { return false; }

        while let Some(Token::And) = sen.next() {
            sen.pop();
            let x = flash(e);
            *e = Exp::And(box x, box Exp::Empty);
            if let Exp::And(_, box ref mut y) = *e {
                // println!("in And_2 : {:?}", sen);
                if !not(sen, y) { return false; }
            }
        }
        true
    }

    pub fn not(sen: &mut TokenStr, e: &mut Exp) -> bool {
        if let Some(Token::Not) = sen.next() {
            // while let Some(Token::Not) = sen.next() { sen.pop(); }
            sen.pop();
            let x = flash(e);

            *e = Exp::Not(box x);
            if let Exp::Not(box ref mut y) = *e {
                return subexp(sen, y);
            } /*else {unreachable!();}*/
        }

        subexp(sen, e)
    }

    pub fn subexp(sen: &mut TokenStr, e: &mut Exp) -> bool {
        atom(sen, e) || parens(sen, e)
    }

    pub fn atom(sen: &mut TokenStr, e: &mut Exp) -> bool {
        if let Some(Token::P(s)) = sen.next() {
            sen.pop();
            if let Some(Token::Lp) = sen.next() {
                sen.pop();
                if let Some(Token::N(i)) = sen.next() {
                    sen.pop();
                    *e = Exp::Pv(s, i);
                    return sen.expect(Token::Rp);
                }
            }
            else {
                *e = Exp:: P(s);
                return true;
            }
        }
        false
    }
}
// } mod parser