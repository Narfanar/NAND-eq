#![allow(
    unused_imports,
    dead_code,
    unused_assignments,
    unused_mut,
    unused_variables,
    deprecated,
    non_snake_case,
    unused_attributes,
)]

#![feature(
    type_ascription,
    box_syntax,
    box_patterns,
    inclusive_range_syntax,
    core_intrinsics,
    pub_restricted,
    slice_patterns,
    // range_contains,
    // str_char,
)]

use std::collections::*;
use std::convert::*;
use std::char::*;
use std::option::*;
use std::mem::*;
use std::mem;
use std::cmp::{max, min};
use std::intrinsics;
use std::cmp;
use std::ops;

mod logos;
mod sets;

fn main() {
    use logos::{lexer as lex, parser as pars};

    let test_vec = vec![
        "x || (y & !z)",
        "~x & (!u)",
        "P(3) || G(0) && ( u >< ~v )",
    ];

    for (i, p) in test_vec.into_iter().enumerate() {
        println!("In[{}]\n= `{}`", i, p);

        let ts = lex::TokenStr::from_str(p.to_owned());
        
        let prop = logos::Prop::from_sentence(ts);
        
        let p2: logos::Prop = prop.clone();
        let b = p2.apply_with(
            |s, i|
                match s {
                    "x" | "y" => Some(true),
                    "z" | "P" => Some(false),
                    _ => None, 
                }
        );
        
        println!("\n`{}` == {}\n", p, b);
        
        println!("Out[fmt]\n= `{}`", prop);
        
        let minimal = prop.clone().minimal_set_form();
        println!("Out[MSF]\n= `{}`", minimal);

        let nand_only = minimal.nand_normal_form();
        println!("Out[NAND]\n= `{}`", nand_only);

        let anf = prop.clone().algebraic_normal_form();
        println!("Out[ANF with NOT]\n= `{}`", anf);

        println!("=====");
    }
}

