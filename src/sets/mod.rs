#![allow(
    unused_imports,
    dead_code,
    unused_assignments,
    unused_mut,
    unused_variables,
    deprecated,
    non_snake_case,
)]

#![feature(
    type_ascription,
    box_syntax,
    box_patterns,
    inclusive_range_syntax,
    core_intrinsics,
    pub_restricted,
    slice_patterns,
    // range_contains,
    // str_char,
)]

use std::collections::*;
use std::convert::*;
use std::char::*;
use std::option::*;
use std::mem::*;
use std::mem;
use std::cmp::{max, min};
use std::intrinsics;
use std::cmp;
use std::ops;


// pub mod sets {
///////////



//////////////
// } mod sets