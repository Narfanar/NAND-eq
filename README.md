# NANDer

* Instructions:
  * You need the Rust language compiler for your OS. Download [here](https://www.rust-lang.org/downloads.html).
  * Install the compiler, then download this repository using the 'Download as zip file' option.
  * Open a terminal window (Cmd on Windows or Terminal on Linux or OS X) and
  change directories (command: `cd <path>` on all platforms) to the path of where you extracted the zip file of this repo.
  * Type `cargo run` and enter.

The program should run and produce output in the same terminal window you used to run it.
Open the source file `src/main.rs` and edit/add input to the input vector in the main
function then recompile to get the new results (this program was first designed for use in
a web environment with no interactive component for active IO—a limitation which persisted
(for now) through the code refactoring and my posting it online).